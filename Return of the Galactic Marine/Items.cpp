#include "Items.h"

HealthPack::HealthPack(float positionX, float positionY) : Item(positionX, positionY)
{
	sizeX = 10.0f;
	sizeY = 10.0f;

	R = 1.0f;
	G = 0.0f;
	B = 0.0f;

	name = "Health Pack";
}
void HealthPack::useItem(int &initialHealth, int &health, int &strength, int &speed)
{
	if(health == initialHealth)
		initialHealth++;
	else
		health = initialHealth;
	hasBeenUsed = true;
}

CombatPack::CombatPack(float positionX, float positionY) : Item(positionX, positionY)
{
	sizeX = 10.0f;
	sizeY = 10.0f;

	R = 0.0f;
	G = 0.0f;
	B = 1.0f;

	name = "Combat Pack";
}
void CombatPack::useItem(int &initialHealth, int &health, int &strength, int &speed)
{
	strength += 2;
	hasBeenUsed = true;
}

Stimulant::Stimulant(float positionX, float positionY) : Item(positionX, positionY)
{
	sizeX = 10.0f;
	sizeY = 10.0f;

	R = 0.0f;
	G = 1.0f;
	B = 0.0f;

	name = "Stimulant";
}
void Stimulant::useItem(int &initialHealth, int &health, int &strength, int &speed)
{
	speed++;
	hasBeenUsed = true;
}