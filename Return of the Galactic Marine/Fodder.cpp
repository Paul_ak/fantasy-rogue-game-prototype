
#include "Game.h"
#include "Fodder.h"

Fodder::Fodder(float positionX, float positionY)
{
	posX = positionX;
	posY = positionY;
	sizeX = 20.0f;
	sizeY = 20.0f;

	R = 0.0f;
	G = 1.0f;
	B = 0.0f;

	health = 3;
	strength = 3;
	speed = 7;
	moneyDrop = 100;
	itemDrop = 40;

	alive = true;
}
Fodder::~Fodder()
{

}