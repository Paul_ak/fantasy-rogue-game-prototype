#ifndef PLAYER_CLASS
#define PLAYER_CLASS

#include <string>
#include <vector>

#include "Enemy.h"
#include "Items.h"
#include "Label.h"

class Player
{
public:
	
	struct Stat
	{
		Stat(int initialValue_arg, std::string statName)
		{
			value = initialValue_arg;
			initialValue = initialValue_arg;
			name = statName;
			spentPoints = 0;
		}

		int value, initialValue, spentPoints;
		std::string name;
	};

	Player(int initialHealth, int initialStrength, int initialSpeed, int initialPoints, int initialDollars);
	~Player();

	void addHealthPoint();
	void addStrengthPoint();
	void addSpeedPoint();
	
	void subtractHealthPoint();
	void subtractStrengthPoint();
	void subtractSpeedPoint();

	void addStat(Stat *stat_arg);
	void subtractStat(Stat *stat_arg);

	void draw();
	void move(bool up, bool down, bool left, bool right);

	void checkCollitions(std::vector<Enemy*> *enemies, std::vector<Item*> *items);

	int dollars, points;
	Stat *health, *strength, *speed;
	Label *dropText;

	bool alive, drawDropText;
	unsigned int dropTextStartMS, drawTextDurationMS;

private:
	float posX, posY, sizeX, sizeY, step, R, G, B;

	void fight(Enemy* enemy);
	void attack(Enemy* enemy);
	void defend(Enemy* enemy);
	void useDrop(int dropType);
};

#endif