#ifndef ENEMY_CLASS
#define ENEMY_CLASS

#include <cstdlib>
#include <GL/glew.h>

enum ItemDrop
{
	NoDrop,
	HealthPackDrop,
	CombarPackDrop,
	StimulantDrop
};

class Enemy
{
protected:
	float R, G, B;

public:
	virtual ~Enemy() { return; }
	void draw()
	{
		if(alive)
		{
			glColor3f(R, G, B);
		
			glBegin(GL_QUADS);
				glVertex2f(posX,  posY);
				glVertex2f(posX + sizeX, posY);
				glVertex2f(posX + sizeX, posY + sizeY);
				glVertex2f(posX,  posY + sizeY);
			glEnd();
		}
	}
	int getDrop()
	{
		int dropChance = rand() % 100;
		if(dropChance > itemDrop)
		{
			dropChance = rand() % 100;

			if(dropChance < 60)			// If number is between 0 - 60
				return HealthPackDrop;
			if(dropChance < 80)			// If number is between 60 - 80
				return CombarPackDrop;
										// If number is between 80 - 100
			return StimulantDrop;

		}
		else
			return NoDrop;
	}

	float posX, posY, sizeX, sizeY;
	int health, strength, speed, moneyDrop, itemDrop;
	bool alive;
};

#endif