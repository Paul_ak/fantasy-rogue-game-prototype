#include <cstdlib>
#include <ctime>
#include <iostream>

#include "Game.h"
#include "GlobalVariables.h"
#include "Player.h"
#include "toString.h"

Player::Player(int initialHealth, int initialStrength, int initialSpeed, int initialPoints, int initialDollars)
{
	//srand(time(0));

	points	= initialPoints;
	dollars = initialDollars;

	alive = true;
	drawDropText = false;

	
	SDL_Color colour = { 255, 255, 255 };
	dropText = new Label(" ", Global::textFont, colour);

	health	 = new Stat(initialHealth, "Health");
	strength = new Stat(initialStrength, "Strength");
	speed	 = new Stat(initialSpeed, "Speed");

	sizeX = 15;
	sizeY = 15;

	posX = Global::screenX / 2;
	posY = Global::screenY / 2;
	step = 1.5f;

	R = 1.0f;
	G = 1.0f;
	B = 1.0f;

	drawTextDurationMS = 2000;
}
Player::~Player()
{
	delete dropText;
	delete strength;
	delete health;
	delete speed;
}

void Player::addHealthPoint()
{
	addStat(health);
}
void Player::addStrengthPoint()
{
	addStat(strength);
}
void Player::addSpeedPoint()
{
	addStat(speed);
}

void Player::subtractHealthPoint()
{
	subtractStat(health);
}
void Player::subtractStrengthPoint()
{
	subtractStat(strength);
}
void Player::subtractSpeedPoint()
{
	subtractStat(speed);
}

void Player::addStat(Stat *stat_arg)
{
	switch(stat_arg->spentPoints)
	{
	case 0:
		if(points > 0)
		{
			points--;
			stat_arg->value++;
			stat_arg->spentPoints++;
		}
		break;
	case 1:
		if(points > 1)
		{
			points -= 2;
			stat_arg->value++;
			stat_arg->spentPoints++;
		}
		break;
	case 2:
		if(points > 3)
		{
			points -= 4;
			stat_arg->value++;
			stat_arg->spentPoints++;
		}
		break;
	}
}

void Player::subtractStat(Stat *stat_arg)
{
	switch(stat_arg->spentPoints)
	{
	case 1:
		points++;
		stat_arg->value--;
		stat_arg->spentPoints--;
		break;
	case 2:
		points += 2;
		stat_arg->value--;
		stat_arg->spentPoints--;
		break;
	case 3:
		points += 4;
		stat_arg->value--;
		stat_arg->spentPoints--;
		break;
	}
}

void Player::draw()
{
    glColor3f(R, G, B);

    glBegin(GL_QUADS);
		glVertex2f(posX,  posY);
		glVertex2f(posX + sizeX, posY);
		glVertex2f(posX + sizeX, posY + sizeY);
		glVertex2f(posX,  posY + sizeY);
	glEnd();

	if(drawDropText)
	{
		if(posY > Global::screenY / 2)
			dropText->draw(posX, posY - 25);
		else
			dropText->draw(posX, posY + 10);
	}
}
void Player::move(bool up, bool down, bool left, bool right)
{
	if(up)
		posY -= step;
	if(down)
		posY += step;
	if(left)
		posX -= step;
	if(right)
		posX += step;

	if(posX < 1)
		posX = 1;
	else
		if(posX > Global::screenX - sizeX)
			posX = Global::screenX - sizeX;

	if(posY < 1)
		posY = 1;
	else
		if(posY > Global::screenY - sizeY)
			posY = Global::screenY - sizeY;
}
void Player::checkCollitions(std::vector<Enemy*> *enemies, std::vector<Item*> *items)
{
	for(int i=0; i < enemies->size(); i++)
	{
		if((*enemies)[i]->alive)
		{
			if(	posX		 > (*enemies)[i]->posX && posX			< (*enemies)[i]->posX + (*enemies)[i]->sizeX &&
				posY		 > (*enemies)[i]->posY && posY			< (*enemies)[i]->posY + (*enemies)[i]->sizeY ||
				posX + sizeX > (*enemies)[i]->posX && posX + sizeX  < (*enemies)[i]->posX + (*enemies)[i]->sizeX &&
				posY		 > (*enemies)[i]->posY && posY			< (*enemies)[i]->posY + (*enemies)[i]->sizeY ||
				posX		 > (*enemies)[i]->posX && posX			< (*enemies)[i]->posX + (*enemies)[i]->sizeX &&
				posY + sizeY > (*enemies)[i]->posY && posY + sizeY  < (*enemies)[i]->posY + (*enemies)[i]->sizeY ||
				posX + sizeX > (*enemies)[i]->posX && posX + sizeX  < (*enemies)[i]->posX + (*enemies)[i]->sizeX &&
				posY + sizeY > (*enemies)[i]->posY && posY + sizeY  < (*enemies)[i]->posY + (*enemies)[i]->sizeY)
			{
				fight((*enemies)[i]);
			}
		}
	}

	for(int i=0; i < items->size(); i++)
	{
		if(!(*items)[i]->hasBeenUsed)
		{
			if(	posX		 > (*items)[i]->posX && posX			< (*items)[i]->posX + (*items)[i]->sizeX &&
				posY		 > (*items)[i]->posY && posY			< (*items)[i]->posY + (*items)[i]->sizeY ||
				posX + sizeX > (*items)[i]->posX && posX + sizeX	< (*items)[i]->posX + (*items)[i]->sizeX &&
				posY		 > (*items)[i]->posY && posY			< (*items)[i]->posY + (*items)[i]->sizeY ||
				posX		 > (*items)[i]->posX && posX			< (*items)[i]->posX + (*items)[i]->sizeX &&
				posY + sizeY > (*items)[i]->posY && posY + sizeY	< (*items)[i]->posY + (*items)[i]->sizeY ||
				posX + sizeX > (*items)[i]->posX && posX + sizeX	< (*items)[i]->posX + (*items)[i]->sizeX &&
				posY + sizeY > (*items)[i]->posY && posY + sizeY	< (*items)[i]->posY + (*items)[i]->sizeY)
			{
				(*items)[i]->useItem(health->initialValue, health->value, strength->value, speed->value);
			}
		}
	}
}

void Player::fight(Enemy* enemy)
{
	if(enemy->speed > speed->value)
		defend(enemy);
	else
		if(enemy->speed < speed->value)
			attack(enemy);
		else
			rand() % 2 == 0 ? attack(enemy) : defend(enemy);
}

void Player::attack(Enemy* enemy)
{
	int playerInitialHealth = health->value;
	while(true)
	{
		enemy->health -= rand() % (strength->value - 1) + 2;

		if(enemy->health <= 0)
		{
			enemy->alive = false;
			health->value += (playerInitialHealth - health->value) / 2;
			dollars += enemy->moneyDrop;
			useDrop(enemy->getDrop());
			break;
		}
		
		health->value -= rand() % (enemy->strength - 1) + 2;

		if(health->value <= 0)
		{
			alive = false;
			break;
		}
	}
}
void Player::defend(Enemy* enemy)
{
	int playerInitialHealth = health->value;
	while(true)
	{
		health->value -= rand() % (enemy->strength - 1) + 2;
		
		if(health->value <= 0)
		{
			alive = false;
			break;
		}
			
		enemy->health -= rand() % (strength->value - 1) + 2;

		if(enemy->health <= 0)
		{
			enemy->alive = false;
			health->value += (playerInitialHealth - health->value) / 2;
			dollars += enemy->moneyDrop;
			useDrop(enemy->getDrop());
			break;
		}
	}
}

void Player::useDrop(int dropType)
{
	Item *drop;
	switch(dropType)
	{
	case NoDrop:
		break;

	case HealthPackDrop:
		drop = new HealthPack(0, 0);
		drop->useItem(health->initialValue, health->value, strength->value, speed->value);
		dropTextStartMS = 0;
		drawDropText = true;
		dropText->setText(("Drop: " + drop->name).c_str(), Global::textFont);
		break;

	case CombarPackDrop:
		drop = new CombatPack(0, 0);
		drop->useItem(health->initialValue, health->value, strength->value, speed->value);
		dropTextStartMS = 0;
		drawDropText = true;
		dropText->setText(("Drop: " + drop->name).c_str(), Global::textFont);
		break;

	case StimulantDrop:
		drop = new Stimulant(0, 0);
		drop->useItem(health->initialValue, health->value, strength->value, speed->value);
		dropTextStartMS = 0;
		drawDropText = true;
		dropText->setText(("Drop: " + drop->name).c_str(), Global::textFont);
		break;
	}
}