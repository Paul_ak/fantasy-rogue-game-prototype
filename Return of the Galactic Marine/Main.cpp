#include <iostream>

#include "Game.h"

int main(int argc, char *argv[])
{
	Game *newGame = new Game();

	newGame->init();
	newGame->run();

	delete newGame;
    return 0;
}