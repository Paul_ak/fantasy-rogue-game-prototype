#ifndef ITEMS_CLASS
#define ITEMS_CLASS

#include <GL/glew.h>
#include <string>

class Item
{
protected:
	float R, G, B;

public:
	std::string name;
	bool hasBeenUsed;
	float posX, posY, sizeX, sizeY;

	Item(float positionX, float positionY)
	{
		//R = R_arg; G = G_arg; B = B_arg;
		posX = positionX; posY = positionY;
		hasBeenUsed = false;
		//sizeX = sizeX_arg; sizeY = sizeY_arg;
	}

	void draw()
	{
		if(!hasBeenUsed)
		{
			glColor3f(R, G, B);
		
			glBegin(GL_QUADS);
				glVertex2f(posX,  posY);
				glVertex2f(posX + sizeX, posY);
				glVertex2f(posX + sizeX, posY + sizeY);
				glVertex2f(posX,  posY + sizeY);
			glEnd();
		}
	}
	virtual void useItem(int &initialHealth, int &health, int &strength, int &speed) = 0;
};

class HealthPack : public Item
{
public:
	HealthPack(float positionX, float positionY);
	void useItem(int &initialHealth, int &health, int &strength, int &speed);
};
class CombatPack : public Item
{
public:
	CombatPack(float positionX, float positionY);
	void useItem(int &initialHealth, int &health, int &strength, int &speed);
};
class Stimulant : public Item
{
public:
	Stimulant(float positionX, float positionY);
	void useItem(int &initialHealth, int &health, int &strength, int &speed);
};

#endif